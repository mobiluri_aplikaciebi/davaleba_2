package com.example.a1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        LogIn.setOnClickListener {
            if (Email.text.toString().isNotEmpty() && Password.text.toString().isNotEmpty()) {
                Toast.makeText(this, "თქვენ წარმატებით შეხვედით", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "გთხოვთ შეავსოთ ყველა ველი", Toast.LENGTH_SHORT).show()
            }


        }
    }
}